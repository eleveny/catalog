<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Catalog;
use App\Http\Requests\CatalogCreateRequest;

class CatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$catalogs = Catalog::all();
		return view('Catalog/index')->with('catalogs',$catalogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user())
        {
            $catalogs = new Catalog;
            return view('Catalog/create')->with('catalog',$catalogs);
        }
        else
        {
           
            return redirect(url('catalog'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CatalogCreateRequest $request)
    {
        //
				Catalog::create([  
				
					'title'=>$request->title,
					'type'=>$request->type,
					'price'=>$request->price,
					'size'=>$request->size,
					'path'=>$request->path	
					]);
					return redirect(url('catalog'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $catalog = Catalog::findOrFail($id);
       return view('Catalog/show')->with('catalog',$catalog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user())
        {
            $catalog = Catalog::findOrFail($id);
            return view('Catalog/edit')->with('catalog',$catalog);
        }
        else
        {
           
            return redirect(url('catalog'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CatalogCreateRequest $request, $id)
    {
         $catalog = Catalog::findOrFail($id);
         $catalog->title = $request->title;
         $catalog->type = $request->type;
         $catalog->price=$request->price;
         $catalog->size = $request->size;
         $catalog->path = $request->path;
         $catalog->save( );
         return redirect(url('catalog'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user())
        {
            $catalog = Catalog::findOrFail($id);
            $catalog->delete();
            return redirect(url('catalog'));
        }
        else
        {
           
            return redirect(url('catalog'));
        }
    }
    
    public function cart()
    {
           return view('catalog/cart');
    }
    
    public function addToCart($id)
    {
        $catalog = Catalog::find($id);
        
               if(!$catalog) {
        
                   abort(404);
        
               }
        
               $cart = session()->get('cart');
        
               // if cart is empty then this the first catalog
               if(!$cart) {
        
                   $cart = [
                           $id => [
                               "title" => $catalog->title,
                               "type" => $catalog->type,
                               "price" => $catalog->price,
                               "size" => $catalog->size,
                               "path" => $catalog->path,
                           ]
                   ];
        
                   session()->put('cart', $cart);
        
                   return redirect()->back()->with('success', 'catalog item added to cart successfully!');
               }
        
               // if cart not empty then check if this catalog exist then increment quantity
               if(isset($cart[$id])) {
        
                   $cart[$id]['quantity']++;
        
                   session()->put('cart', $cart);
        
                   return redirect()->back()->with('success', 'catalog item added to cart successfully!');
        
               }
        
               // if item not exist in cart then add to cart with quantity = 1
               $cart[$id] = [
                   "title" => $catalog->title,
                   "type" => $catalog->type,
                   "price" => $catalog->price,
                   "size" => $catalog->size,
                   "path" => $catalog->path,
               ];
        
               session()->put('cart', $cart);
        
               return redirect()->back()->with('success', 'catalog item added to cart successfully!');
    }
    
    public function about()
    {
        return view('Catalog/about');
    }
}
