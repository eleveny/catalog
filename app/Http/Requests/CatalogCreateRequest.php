<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CatalogCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'title' => 'required|regex:^[A-Z0-9][A-Za-z0-9]*^',
 	 'type' => 'required',
	 'size' => 'required',
	 'price' => 'required|numeric',
         'path' => 'required',
	    
        ];
    }
}
