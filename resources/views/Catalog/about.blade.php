<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
       

            <div class="content">
                <div class="title m-b-md">
                    About Us
                </div>

                <div >
                    <p> Caribbean culture and heritage is reflection of the region�s rich history.With art being a large part of that history and culture there has been a vast production of paintings, 
					    sculptures and prints. One such country where this holds true is Barbados. With a passion for cultural identity and art we created
						Chattel Art Rentals.Chattel Art Rentals was established in September of 2019. Our founders are Wayne Scantlebury, Issabi Cobham, 
						Ebony Bentham, Shane Proverbs and Gary Smith.We strive to offer quality service and quality art pieces to our customers for their desired use </p>
                </div>
            </div>
        </div>
    </body>
</html>
