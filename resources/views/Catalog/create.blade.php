<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 50vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    @include('Catalog.menu')

    </head>
    <body>

       <div class="full-height">
            @if (Route::has('login'))
                <div class="top-left links">
                    @auth
                        <a href="{{ url('/home') }}"><span style="color:#FFC300; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:44px;">C</span><span style="font-size:30px;">H</span><span style="font-size:20px;">a</span><span style="font-size:18px;">t</span><span style="font-size:16px;">t</span>el 
						A<span style="font-size:24px;">r</span><span style="font-size:32px;">T</span></span> <span style ="color:#2D56FF; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:40px;">R</span>entals</span></a>
                   
													
                         <a id="bnav" class="top-right"  href="{{ url('/logout') }}">Logout</a>
				   @else
						 <a href="{{ url('/') }}"><span style="color:#FFC300; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:44px;">C</span><span style="font-size:30px;">H</span><span style="font-size:20px;">a</span><span style="font-size:18px;">t</span><span style="font-size:16px;">t</span>el 
					  A<span style="font-size:24px;">r</span><span style="font-size:32px;">T</span></span> <span style ="color:#2D56FF; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:40px;">R</span>entals</span></a>
                     

					 <a id="bnav" class="top-right"  href="{{ route('login') }}">Login</a>
                        
                    
                        
                    @endauth
                </div>
            @endif
        
			

    <div class="content">
        <div class="title m-b-md">
            Create
        </div>

		@if($errors->any())
                  @foreach($errors->all( ) as $error)
            	     <p>{{$error}} </p>
		@endforeach
		@endif

@guest
<?php
return redirect()->route('/home');
    ?>
@endauth

        @auth
                {!! Form::open(['url' => '/catalog']) !!}

                {!! Form::label ('title', 'Title: ') !!}
                {!! Form::text('title', $catalog->title) !!}

                {!! Form::label ('type', 'Type: ') !!}
                {!! Form::text('type', $catalog->type) !!}

                {!! Form::label ('price', 'Price: ') !!}
                {!! Form::text('price', $catalog->price) !!}

                {!! Form::label ('size', 'Size: ') !!}
                {!! Form::text('size', $catalog->size) !!}

				{!! Form::label('imagePath','imagePath: ') !!}
				{!! Form::text('path', $catalog->path) !!}

                {!! Form::submit('Save') !!}

                {!! Form::close() !!}

        @endauth
    </div>
    </div>

    </body>
</html>
