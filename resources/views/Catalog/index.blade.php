<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Chattel Art Rentals Catalog</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link href="css/mystyle.css" rel="stylesheet" type="text/css">
        <!-- Styles -->
    @include('Catalog.menu')
    </head>
    <body>
	
       <div class="full-height">
            @if (Route::has('login'))
                <div class="links">
                    @auth
                        <a href="{{ url('/home') }}"><span style="color:#FFC300; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:44px;">C</span><span style="font-size:30px;">H</span><span style="font-size:20px;">a</span><span style="font-size:18px;">t</span><span style="font-size:16px;">t</span>el 
						A<span style="font-size:24px;">r</span><span style="font-size:32px;">T</span></span> <span style ="color:#2D56FF; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:40px;">R</span>entals</span></a>
                     
					<a id="bnav" class="top-right"  href="{{ route('logout')}}">LogOut</a>
					@else
						 <a href="{{ url('/') }}"><span style="color:#FFC300; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:44px;">C</span><span style="font-size:30px;">H</span><span style="font-size:20px;">a</span><span style="font-size:18px;">t</span><span style="font-size:16px;">t</span>el 
					  A<span style="font-size:24px;">r</span><span style="font-size:32px;">T</span></span> <span style ="color:#2D56FF; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:40px;">R</span>entals</span></a>
                     

					 <a id="bnav" class="top-right"  href="{{ route('login') }}">Login</a>

                        
                    @endauth
                </div>
            @endif
			
    </div>



            <div class="content">
                <div class="title m-b-md ">
                  <h5 id="Ctext" > Chattel Arts Catalog </h5>
                </div>



				@auth {{--Authentication starts: only logged in users will see what is enclosed in auth--}}
                <a id="create" href="{{ url('catalog/create') }}"{!! Form::button('Add to Catalog') !!}</a>{{--This button redirects the user to the page to create a new resource--}}
                @endauth {{--Aunthentication ends--}}


                <ul> {{--list starts--}}

               
 		@foreach ($catalogs as $catalog) {{--foreach begins--}}

               	 <div class="card"> {{--Each catalog item is placed in a card--}}

			
                    
                      <img src="images/{{ $catalog->path }}" alt="Cat" style="width:100%; height:200px;">  {{--This is the catalog item's image--}}
                      {{ $catalog->title }} {{--This is the catalog item's title--}}
                      <p class="price">${{ $catalog->price }}</p> {{--This is the catalog item's price--}}
                      {{ $catalog->type }}<br> {{--This is the catalog item's type--}}
                      {{ $catalog->size }} {{--This is the catalog item's size--}}
                          
                      {!! Form::open(['method' => 'DELETE', 'url' => 'catalog/' . $catalog->id]) !!} {{--Form opens here--}}
                    
                     {!! Form::button('Add to Cart',['id'=>'addtoCart'])!!}  {{--This is the 'add to cart' button, but it has no functionality at the moment--}}
                          
                      @auth {{--Authentication starts: only logged in users will see what is enclosed in auth--}}
                          {!! Form::button('Delete',['type'=>'submit','id'=>'delete']) !!} {{--This button deletes the resource --}}
                       
				<button><a href="{{ url('catalog/' . $catalog->id . '/edit') }}"{!! Form::button('Edit Catalog Item',["id"=>"edit"]) !!}</a></button>{{--This button redirects the user to the page to edit the resource--}}
                      
					  
					  @endauth {{--Authentication ends--}}
                          
                      {!! Form::close() !!} {{--Form ends here--}}

                </div>
				{{--End of card division--}}

                @endforeach {{--foreach ends--}}
              
                </ul> {{--list ends--}}
                
            </div>
        </div>
    </body>
</html>
