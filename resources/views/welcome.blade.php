<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Chattel Art Rentals Welcome</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link href="css/mystyle.css" rel="stylesheet" type="text/css">
        <!-- Styles -->
    
    </head>
    <body>
        <div class="full-height">
            @if (Route::has('login'))
                <div class="links">
                    @auth
                        <a href="{{ url('/home') }}"><span style="color:#FFC300; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:44px;">C</span><span style="font-size:30px;">H</span><span style="font-size:20px;">a</span><span style="font-size:18px;">t</span><span style="font-size:16px;">t</span>el 
						A<span style="font-size:24px;">r</span><span style="font-size:32px;">T</span></span> <span style ="color:#2D56FF; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:40px;">R</span>entals</span></a>
                   <a id="bnav" class="top-right"  href="{{ route('logout')}}">LogOut</a>
				   @else
						 <a href="{{ url('/') }}"><span style="color:#FFC300; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:44px;">C</span><span style="font-size:30px;">H</span><span style="font-size:20px;">a</span><span style="font-size:18px;">t</span><span style="font-size:16px;">t</span>el 
					  A<span style="font-size:24px;">r</span><span style="font-size:32px;">T</span></span> <span style ="color:#2D56FF; text-shadow: 0.2em 0.2em 0.2em rgba(0,0,0,0.2);"><span style="font-size:40px;">R</span>entals</span></a>
                     

					 <a id="bnav" class="top-right"  href="{{ route('login') }}">Login</a>

                        
                    @endauth
                </div>
            @endif
			
    </div>


	
            <div class="content">
                <div class="title m-b-md">
                    Welcome to Chattel Arts
				       		 				
                </div>
				
				
<a style="color:white;text-decoration:none;font-size:30px;" href="{{ url('catalog/') }}"{!! Form::button('Browse Art Catalog' , ['id'=>'browse']) !!}</a>
				
<h1 style="color:blue;margin-top:150px; font-size:54px;"><span style="color:#FFC300;">ABOUT</span> US</h1>

<p style="margin-top:30px;font-size:18px;margin:0px 40px 0px 40px;"> Caribbean culture and heritage is reflection of the regionís rich history.With art being a large part of that history and culture there has been a vast production of paintings,
                sculptures and prints. One such country where this holds true is Barbados. With a passion for cultural identity and art we created
                Chattel Art Rentals.Chattel Art Rentals was established in September of 2019. Our founders are Wayne Scantlebury, Issabi Cobham,
                Ebony Bentham, Shane Proverbs and Gary Smith.We strive to offer quality service and quality art pieces to our customers for their desired use </p>
				
                </div>
        
       
    </body>
</html>
